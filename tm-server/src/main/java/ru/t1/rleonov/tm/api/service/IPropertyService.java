package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getDbSecondLvlCash();

    @NotNull
    String getDbSchema();

    @NotNull
    String getDbFactoryClass();

    @NotNull
    String getDbUseQueryCash();

    @NotNull
    String getDbUseMinPuts();

    @NotNull
    String getDbRegionPrefix();

    @NotNull
    String getDbHazelConfig();

    @NotNull
    String getDbDialect();

    @NotNull
    String getDbDdlAuto();

    @NotNull
    String getDbShowSql();

    @NotNull
    String getDbFormatSql();

    @NotNull
    String getDbDriver();

    @NotNull
    String getDbLogin();

    @NotNull
    String getDbPassword();

    @NotNull
    String getDbUrl();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
