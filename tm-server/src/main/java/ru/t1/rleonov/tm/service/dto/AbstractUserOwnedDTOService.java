package ru.t1.rleonov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.rleonov.tm.api.repository.dto.IDTORepository;
import ru.t1.rleonov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.rleonov.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.rleonov.tm.enumerated.Sort;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO,
        R extends IUserOwnedDTORepository<M> & IDTORepository<M>>
        extends AbstractDTOService<M, R>
        implements IUserOwnedDTOService<M> {

    @NotNull
    protected String getSortType(@Nullable final Sort sort) {
        if (sort == Sort.BY_CREATED) return "created";
        if (sort == Sort.BY_STATUS) return "status";
        else return "name";
    }

}
