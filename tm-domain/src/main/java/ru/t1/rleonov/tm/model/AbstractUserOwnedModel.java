package ru.t1.rleonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.model.IWBS;
import ru.t1.rleonov.tm.enumerated.Status;
import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractModel implements IWBS {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    @NotNull
    protected String name = "";

    @Column
    @NotNull
    protected String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    private Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return name + " : " + status + " : " + description;
    }

}
