package ru.t1.rleonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.api.endpoint.*;
import ru.t1.rleonov.tm.api.repository.ICommandRepository;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.command.AbstractCommand;
import ru.t1.rleonov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.rleonov.tm.exception.system.CommandNotSupportedException;
import ru.t1.rleonov.tm.util.SystemUtil;
import ru.t1.rleonov.tm.util.TerminalUtil;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private AbstractCommand[] abstractCommands;

    @Getter
    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Getter
    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initCommands(@NotNull final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command: commands) {
            commandService.add(command);
        }
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                processCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, final boolean checkRoles) {
        if (command == null) throw new CommandNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processArguments(@NotNull final String[] args) {
        @NotNull final String arg = args[0];
        try {
            processArgument(arg);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
        }
    }

    private void processArgument(@Nullable final String arg) {
        if (arg == null) throw new ArgumentNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void prepareStartup() {
        initPID();
        initCommands(abstractCommands);
        loggerService.info("***WELCOME TO TASK-MANAGER***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("***TASK-MANAGER IS SHUTTING DOWN***");
        fileScanner.stop();
    }

    public void run(@Nullable final String[] args) {
        prepareStartup();
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

}
