package ru.t1.rleonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ProjectShowByIdRequest;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String COMMAND = "project-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Show project by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);
        @Nullable final ProjectDTO project = getProjectEndpoint().showProjectById(request).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
